package com.bonefire.inventory.management.system.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bonefire.inventory.management.system.beans.User;
import com.bonefire.inventory.management.system.repository.UserRepository;

@RestController
@RequestMapping(value = "/rest/user")
public class UserController {

	@Autowired
	private UserRepository userRepository;

	@GetMapping(value = "/all")
	public List<User> getAll() {
		return userRepository.findAll();
	}

	@PostMapping(value = "/create")
	public List<User> persist(@RequestBody User user) {

		userRepository.save(user);

		return userRepository.findAll();
		
	}
}
