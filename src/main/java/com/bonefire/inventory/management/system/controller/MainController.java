package com.bonefire.inventory.management.system.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

	@GetMapping("/")
	public String welcomePage() {
		
		return "Hi, welcome to the world of programming";
	}
	
}
