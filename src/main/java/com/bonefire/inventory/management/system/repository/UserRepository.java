package com.bonefire.inventory.management.system.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bonefire.inventory.management.system.beans.User;

public interface UserRepository extends JpaRepository<User, Integer>{

}
